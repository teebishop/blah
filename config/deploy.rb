require 'net/http'

lock '3.2.1' # version lock

set :application, 'miked'
set :deploy_to, '/opt/miked'
set :linked_dirs, %w{log tmp/pids tmp/sockets}

set :repo_url, 'https://teebishop@bitbucket.org/teebishop/blah.git'
set :branch, ENV['branch'] || 'master'

set :ssh_options, {forward_agent: false}

set :user, 'ubuntu'

namespace :deploy do
  desc 'Start Unicorn'
  task :start do
    on roles(:app), in: :sequence, wait: 5 do
      sudo 'service miked-unicorn start'
    end
  end

  desc 'Stop Unicorn'
  task :stop do
    on roles(:app), in: :sequence, wait: 5 do
      sudo 'service miked-unicorn stop'
    end
  end

  desc 'Restart (Reload) Unicorn'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      pidfile = shared_path.join('tmp/pids/unicorn.pid')
      if test "[ -f #{pidfile} ]"
        sudo 'service miked-unicorn reload'
      else
        sudo 'service miked-unicorn start'
      end
      sleep 5
    end
  end

  after :publishing, :restart
  after :finishing, :cleanup
end

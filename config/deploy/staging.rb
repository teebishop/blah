set :stage, :staging
set :rails_env, 'staging'

cap_host = ENV['CAP_HOST'] || '54.89.102.110'
set :cap_host, cap_host

vagrant_key = "#{ENV['HOME']}/.vagrant.d/insecure_private_key"
set :vagrant_key, vagrant_key

server fetch(:cap_host),
       user: fetch(:user),
       roles: %w{web app db},
       primary: true,
       port: 22,
       ssh_options: {
         keys: fetch(:vagrant_key),
         auth_methods: %w(publickey),
         forward_agent: 'yes'
       }
